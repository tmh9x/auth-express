const express = require("express");
const session = require("express-session");
const bodyParser = require("body-parser");

const app = express();

app.use(bodyParser.json());
app.use(
  session({
    secret: "secret",
    resave: true,
    saveUninitialized: true,
  })
);

const users = [
  { username: "user1", password: "pw1" },
  { username: "user2", password: "pw2" },
];

app.post("/login", (req, res) => {
  const { username, password } = req.body;
  const user = users.find(
    (u) => u.username === username && u.password === password
  );

  if (user) {
    req.session.user = user;
    console.log("SESSION", req.session);
    console.log("req.session.user", req.session.user);
    req.session.save((err) => {
      if (err) {
        console.error("Error saving session:", err);
        res.status(500).send("Error saving session");
      } else {
        console.log("HIER", req.session.user);
        res.send("Login successful");
      }
    });
  } else {
    res.status(401).send("Invalid credentials");
  }
});

app.get("/check-session", (req, res) => {
  console.log("req.session", req.session);
  console.log("req.session.user", req.session.user);
  if (req.session && req.session.user) {
    res.send("Session is active");
  } else if (req.headers.cookie && req.headers.cookie.includes("connect.sid")) {
    res.send("Session cookie is present but not active");
  } else {
    res.send("No active session");
  }
});

app.get("/logout", (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      res.status(500).send("Error logging out");
    } else {
      res.send("Logout successful");
    }
  });
});

const PORT = 3000;

const server = app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

module.exports = app;
